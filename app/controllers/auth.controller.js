const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const refreshTokenService = require("../services/refreshToken.service")
const userModel = require("../model/user.model")
const roleModel = require("../model/role.model")
const refreshTokenModel = require("../model/refreshToken.model")
const mongoose = require("mongoose")

const signUp = async (req, res) => {
    const { username, phone, email, password } = req.body
    const roleName = req.query.roleName==="admin"?"admin":"user"
    const role = await roleModel.findOne({
        name: roleName
    })
    try {
        const newUser = {
            username,
            password: bcrypt.hashSync(password, 8),
            phone,
            email,
            role: role._id
        }
        if (!username) {
            return res.status(400).json({
                message: "Username is require"
            })
        }
        const findDublicateUsername = userModel.find({username:username})
        if((await findDublicateUsername).length>0){
            return res.status(400).json({
                message: "Username is dublicate"
            })
        }
        if (!password) {
            return res.status(400).json({
                message: "Password is require"
            })
        }
        if (isEmailValidate(email) == false) {
            return res.status(400).json({
                message: "Email invalid"
            })
        }
        const findDublicateEmail = userModel.find({email:email})
        if((await findDublicateEmail).length>0){
            return res.status(400).json({
                message: "Email is dublicate"
            })
        }
        if (isPhoneValidate(phone) == false) {
            return res.status(400).json({
                message: "Phone invalid"
            })
        }
        const findDublicatePhone = userModel.find({phone:phone})
        if((await findDublicatePhone).length>0){
            return res.status(400).json({
                message: "Phone is dublicate"
            })
        }
        const data = await userModel.create(newUser)
        return res.status(201).json({
            message: "Create user successfully"
        })
    } catch (error) {
        res.status(500).json({
            message: "Internal servel error", error
        })
    }
}
const logIn = async (req, res) => {
    const { username, password } = req.body
    try {
        const existUser = await userModel.findOne({
            username
        })
        if (!existUser) {
            return res.status(404).json({
                message: "User not found"
            })
        }
        var passwordIsValid = bcrypt.compareSync(
            password,
            existUser.password
        )
        if (!passwordIsValid) {
            return res.status(400).json({
                message: "Password Invalid"
            })
        }
        const secretKey = "YouCanNotHackMyPassword"
        const token = await jwt.sign({
            id: existUser._id,
        },
            secretKey,
            {
                algorithm: "HS256",
                allowInsecureKeySizes: true,
                expiresIn: 7200
            }
        )
        //Sinh thêm refresh token
        const refreshToken = await refreshTokenService.createToken(existUser)
        return res.status(200).json({
            accessToken: token,
            refreshToken: refreshToken
        })
    } catch (error) {
        res.status(500).json({
            message: "Internal servel error", error
        })
    }
}
const refreshToken = async (req, res) => {
    const refreshToken = req.body.refreshToken
    if (!refreshToken) {
        return res.status(403).json({
            message: " Request Token is required"
        })
    }
    try {
        const refreshTokenObj = await refreshTokenModel.findOne({
            token: refreshToken
        })
        if (!refreshTokenObj) {
            return res.status(403).json({
                message: "Request token not found"
            })
        }
        // Nếu req token hết hạn
        if (refreshTokenObj.expiredDate.getTime() < new Date().getTime()) {
            // xóa req token trong csdl
            await refreshTokenModel.findByIdAndRemove(refreshTokenObj._id)
            return res.status(403).json({
                message: " Request Token was expired!"
            })
        }
        const secretKey = "YouCanNotHackMyPassword"
        const newAccessToken = jwt.sign({
            id: refreshTokenObj.user,
        },
            secretKey,
            {
                algorithm: "HS256",
                allowInsecureKeySizes: true,
                expiresIn: 86000
            }
        )
        res.status(200).json({
            accessToken: newAccessToken,
            refreshToken: refreshTokenObj.token
        })

    } catch (error) {
        res.status(500).json({
            message: "Internal servel error", error
        })
    }
}
// Hàm lấy all user
const getAllUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    const username = req.query.username
    const phone = req.query.phone
    const email = req.query.email

    const condition = {}
    // B2 Kiểm tra dữ liệu
    if (phone) {
        condition.phone = phone
    }
    if (username) {
        condition.username = username
    }
    if (email) {
        condition.email = email
    }
    // B3 Xử lý API
    try {
        var result = await userModel.findOne(condition)
        return res.status(200).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm lấy user bằng id
const getUserById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const userId = req.params.userId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // B3 Xử lý API
    try {
        var result = await userModel.findById(userId).populate("role")
        return res.status(200).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//Hàm kiểm tra email
function isEmailValidate(paramEmail) {
    var vRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return vRegex.test(paramEmail)
}
//Hàm kiểm tra SĐT
function isPhoneValidate(paramPhone) {
    var regexPhoneNumber = /(84|0[1|3|7|9])+([0-9]{8})\b/g;
    return paramPhone.match(regexPhoneNumber) ? true : false;
}
module.exports = { signUp, logIn, refreshToken, getUserById, getAllUser }