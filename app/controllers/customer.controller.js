//Import router
const customerModel = require("../model/customer.model")
const router = require("../routes/customer.router")
// Import Thư viện Mongoose
const mongoose = require("mongoose")
// Hàm tạo customer
const createCustomer = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { fullName, phone, email, address, city, country, orders } = req.body
    // B2 Kiểm tra dữ liệu
    if (!fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required"
        })
    }

    // B3 Xử lý API
    const newCustomer = {
        _id: new mongoose.Types.ObjectId,
        fullName,
        phone,
        email,
        address,
        city,
        country,
        orders
    }
    try {
        var result = await customerModel.create(newCustomer)
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm lấy danh sách customer
const getAllCustomer = async (req, res) => {
    // B1 Thu thập dữ liệu
    const phone = req.query.phone?req.query.phone:false
    const email = req.query.email?req.query.email:false
    const fullName = req.query.fullName?req.query.fullName:false
    const limit = req.query.limit
    const page = req.query.page
    const condition = {}
    if(phone){
        condition.phone = { "$regex": phone }
    }
     if(email){
        condition.email = { "$regex": email }
    }   
     if(fullName){
        condition.fullName = { "$regex": fullName }
    }  
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý API
    try {
        var result = await customerModel.find(condition).limit(limit?limit:0).skip((page-1)*limit)
        .skip(page>0?((page-1)*limit):0)
        return res.status(200).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm lấy customer bằng id
const getCustomerById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const customerId = req.params.customerId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // B3 Xử lý API
    try {
        var result = await customerModel.findById(customerId)
        return res.status(200).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm update customer bằng id
const updateCustomerById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const customerId = req.params.customerId
    const { fullName, phone, email, address, city, country, orders } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    if (!fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required"
        })
    }
    // B3 Xử lý API
    const updateCustomer = {
        fullName,
        phone,
        email,
        address,
        city,
        country,
        orders
    }
    try {
        var result = await customerModel.findByIdAndUpdate(customerId, updateCustomer)
        return res.status(200).json({
            message: "update successfully",
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm xóa customer bằng id
const deleteCustomerById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const customerId = req.params.customerId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // B3 Xử lý API
    try {
        var result = await customerModel.findByIdAndDelete(customerId)
        return res.status(200).json({
            message: "delete successfully",
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}


// Exports
module.exports = { createCustomer, getAllCustomer, getCustomerById, updateCustomerById, deleteCustomerById }