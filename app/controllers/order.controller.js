//Import router
const orderModel = require("../model/order.model")
const customerModel = require("../model/customer.model")
const router = require("../routes/order.router")
// Import Thư viện Mongoose
const mongoose = require("mongoose")
// Hàm tạo order và add vào customer
const createOrderOfCustomer = async (req, res) => {
    // B1 Thu thập dữ liệu
    customerId = req.params.customerId
    const { orderDate, shippedDate, note, orderDetails, cost,orderCode } = req.body
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý API
    const newOrder = {
        _id: new mongoose.Types.ObjectId,
        orderDate,
        shippedDate,
        note,
        orderDetails,
        cost,
        orderCode
    }
    try {
        var result = await orderModel.create(newOrder)
        var addOrder = await customerModel.findByIdAndUpdate(customerId, {
            $push: { orders: result._id }
        })
        return res.status(201).json({
            result,
            addOrder
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm lấy danh sách order
const getAllOrder = async (req, res) => {
    // B1 Thu thập dữ liệu
    const limit = req.query.limit
    const page = req.query.page
    const cost = req.query.cost?Number(req.query.cost):false
    const code = req.query.code?req.query.code:false
    const itemNumbers = req.query.itemNumbers?req.query.itemNumbers:false
    // B2 Kiểm tra dữ liệu
    const condition = {}
    if(cost){
        condition.cost=cost
    }
    if(code){
        condition.orderCode={ "$regex": code }
    }
    try {
        var result = await orderModel.find(condition).limit(limit?limit:0).skip((page-1)*limit).populate("orderDetails")
        return res.status(200).json({
            result,
            product:result.orderDetails
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm lấy danh sách order của customer theo id
const getAllOrderOfCustomer = async (req, res) => {
    // B1 Thu thập dữ liệu
    customerId = req.params.customerId
    
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý API
    try {
        var result = await customerModel.findById(customerId)
        return res.status(200).json({
            result: result.orders
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm lấy order bằng id
const getOrderById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const orderId = req.params.orderId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // B3 Xử lý API
    try {
        var result = await orderModel.findById(orderId)
        return res.status(200).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm update order bằng id
const updateOrderById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const orderId = req.params.orderId
    const { shippedDate, note, orderDetails, cost } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // B3 Xử lý API
    const updateOrder = {
        shippedDate,
        note,
        orderDetails,
        cost
    }
    try {
        var result = await orderModel.findByIdAndUpdate(orderId, updateOrder)
        return res.status(200).json({
            message: "update successfully",
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm xóa order bằng id
const deleteOrderById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const orderId = req.params.orderId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // B3 Xử lý API
    try {
        var result = await orderModel.findByIdAndDelete(orderId)
        return res.status(200).json({
            message: "delete successfully",
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}


// Exports
module.exports = { createOrderOfCustomer, getAllOrder, getAllOrderOfCustomer, getOrderById, updateOrderById, deleteOrderById }