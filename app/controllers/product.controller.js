//Import router
const productModel = require("../model/product.model")
const router = require("../routes/product.router")
// Import Thư viện Mongoose
const mongoose = require("mongoose")
// Hàm tạo productType
const createProduct = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { name, description, type, imageUrl, buyPrice, promotionPrice, amount } = req.body
    // B2 Kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required"
        })
    }
    if (!type) {
        return res.status(400).json({
            status: "Bad request",
            message: "type is required"
        })
    }
    if (!imageUrl) {
        return res.status(400).json({
            status: "Bad request",
            message: "imageUrl is required"
        })
    }
    if (!buyPrice) {
        return res.status(400).json({
            status: "Bad request",
            message: "buyPrice is required"
        })
    }
    if (!promotionPrice) {
        return res.status(400).json({
            status: "Bad request",
            message: "promotionPrice is required"
        })
    }
    // B3 Xử lý API
    const newProduct = {
        _id: new mongoose.Types.ObjectId,
        name,
        description,
        imageUrl,
        buyPrice,
        promotionPrice,
        amount,
        type
    }
    try {
        var result = await productModel.create(newProduct)
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm lấy danh sách product
const getAllProduct = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý API
    const limit = req.query.limit ? req.query.limit : 0
    const min = req.query.min ? req.query.min : 0
    const max = req.query.max ? req.query.max : 2000
    const name = req.query.name?req.query.name:false
    const amount = req.query.amount?req.query.amount:false
    const buyPrice = req.query.buyPrice?req.query.buyPrice:false
    const promotionPrice = req.query.promotionPrice?req.query.promotionPrice:false
    const page = req.query.page?req.query.page:1
    const type1 = req.query.type1? req.query.type1:false
    const type2 = req.query.type2? req.query.type2:false
    const type3 = req.query.type3? req.query.type3:false
    const type4 = req.query.type4? req.query.type4:false
    const type5 = req.query.type5? req.query.type5:false
    const type6 = req.query.type6? req.query.type6:false
    var condition = {
    }

    if(type1||type2||type3||type4||type5||type6){
        condition.type=[]
    }
    if(type1){
        condition.type.push(type1)
    }
    if(type2){
        condition.type.push(type2)
    }
    if(type3){
        condition.type.push(type3)
    }
    if(type4){
        condition.type.push(type4)
    }
    if(type5){
        condition.type.push(type5)
    }
    if(type6){
        condition.type.push(type6)
    }
    condition.promotionPrice = { "$gte": min, "$lte": max }
    if (name) {
        condition.name = { "$regex": name }
    }
    if (amount) {
        condition.amount = amount
    }
    if (buyPrice) {
        condition.buyPrice = buyPrice 
    }
    if (promotionPrice) {
        condition.promotionPrice =promotionPrice
    }
    try {
        var result;
        if (limit) {
            result = await productModel.find(condition).limit(limit).skip((page-1)*limit).populate("type")
        }
         if(type1||type2||type3||type4||type5){
            result = await productModel.find(condition).populate("type")
        }
        if(type6){
            result = await productModel.find().populate("type").limit().sort({type:-1})
        }
        else{
            result = await productModel.find(condition).limit(limit).skip((page-1)*limit).populate("type")
        }
        return res.status(200).json({
            result
        });
    } catch (error) {

        return res.status(500).json({
            status: "Internal Server Error",
            message: error
        })
    }
}
// Hàm lấy type bằng id
const getProductById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const productId = req.params.productId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // B3 Xử lý API
    try {
        var result = await productModel.findById(productId)
        return res.status(200).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm update type bằng id
const updateProductById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const productId = req.params.productId
    const { name, description, type, imageUrl, buyPrice, promotionPrice, amount } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // if (!name) {
    //     return res.status(400).json({
    //         status: "Bad request",
    //         message: "name is required"
    //     })
    // }
    // if (!type) {
    //     return res.status(400).json({
    //         status: "Bad request",
    //         message: "type is required"
    //     })
    // }
    // if (!imageUrl) {
    //     return res.status(400).json({
    //         status: "Bad request",
    //         message: "imageUrl is required"
    //     })
    // }
    // if (!buyPrice) {
    //     return res.status(400).json({
    //         status: "Bad request",
    //         message: "buyPrice is required"
    //     })
    // }
    // if (!promotionPrice) {
    //     return res.status(400).json({
    //         status: "Bad request",
    //         message: "promotionPrice is required"
    //     })
    // }
    // B3 Xử lý API
    const updateProduct = {
        name,
        description,
        imageUrl,
        buyPrice,
        promotionPrice,
        amount,
        type
    }
    try {
        var result = await productModel.findByIdAndUpdate(productId, updateProduct)
        return res.status(200).json({
            message: "update successfully",
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm xóa type bằng id
const deleteProductById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const productId = req.params.productId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // B3 Xử lý API
    try {
        var result = await productModel.findByIdAndDelete(productId)
        return res.status(200).json({
            message: "delete successfully",
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}


// Exports
module.exports = { createProduct, getAllProduct, getProductById, updateProductById, deleteProductById }