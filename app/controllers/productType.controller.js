//Import router
const productTypeModel = require("../model/productType.model")
const router = require("../routes/productType.router")
// Import Thư viện Mongoose
const mongoose = require("mongoose")
// Hàm tạo productType
const createProductType = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { name, description } = req.body
    // B2 Kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required"
        })
    }
    // B3 Xử lý API
    const newProductType = {
        _id: new mongoose.Types.ObjectId,
        name,
        description
    }
    try {
        var result = await productTypeModel.create(newProductType)
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm lấy danh sách type
const getAllType = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý API
    try {
        var result = await productTypeModel.find()
        return res.status(200).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm lấy type bằng id
const getTypeById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const typeId = req.params.typeId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(typeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // B3 Xử lý API
    try {
        var result = await productTypeModel.findById(typeId)
        return res.status(200).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm update type bằng id
const updateTypeById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const typeId = req.params.typeId
    const { name, description } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(typeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required"
        })
    }
    // B3 Xử lý API
    const updateType = {
        name,
        description
    }
    try {
        var result = await productTypeModel.findByIdAndUpdate(typeId, updateType)
        return res.status(200).json({
            message: "update successfully",
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm xóa type bằng id
const deleteTypeById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const typeId = req.params.typeId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(typeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // B3 Xử lý API
    try {
        var result = await productTypeModel.findByIdAndDelete(typeId)
        return res.status(200).json({
            message: "delete successfully",
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}


// Exports
module.exports = { createProductType, getAllType, getTypeById, updateTypeById, deleteTypeById }