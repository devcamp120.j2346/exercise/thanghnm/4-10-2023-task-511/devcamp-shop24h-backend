const roleModel = require("../model/role.model")

const createRole = async(req,res)=>{
    try {
        const role={
            name:req.body.name
        }
        const data = await roleModel.create(role)
        return res.status(201).json({
            data
        })
    } catch (error) {
        return res.status(500).json({
            message:"Internal server error"
        })
    }

}
module.exports={createRole}