const userModel = require("../model/user.model")
const jwt = require("jsonwebtoken")
require("dotenv")
const checkDublicateUsername = async (req, res, next) => {
    try {
        const existUser = await userModel.findOne({
            username: req.body.username
        })
        if (existUser) {
            res.status(400).send({
                message: "Username is already in use"
            })
        }
        const existEmail = await userModel.findOne({
            email: req.body.email
        })
        if (existEmail) {
            res.status(400).send({
                message: "Email is already in use"
            })
        }
        const existPhone = await userModel.findOne({
            email: req.body.phone
        })
        if (existEmail) {
            res.status(400).send({
                message: "Phone is already in use"
            })
        }
        next()
    } catch (error) {
        console.error("Internal server error", error)
        process.exit()
    }
}
const verifyToken = async (req, res, next) => {
    try {
        console.log("verify token...")
        const token = req.headers.authorization.split(" ")[1]
        if (!token) {
            return res.status(404).json({
                message: "Token not found"
            })
        }
        const secretKey = "YouCanNotHackMyPassword"
        const verified =  jwt.verify(token,secretKey)
        if (!verified) {
            return res.status(404).json({
                message: "Token invalid"
            })
        }
        const user = await userModel.findById(verified.id).populate("role")
        // console.log(user)
        req.user = user
        next()
    } catch (error) {
        console.error("Internal server error", error)
        process.exit()
    }
}
module.exports = {checkDublicateUsername, verifyToken}