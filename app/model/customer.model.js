// Import Thư viện Express
const mongoose = require("mongoose")
// Khai báo Schema
const Schema = mongoose.Schema
// Khởi tạo schema
const customerSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        default: ""
    },
    city: {
        type: String,
        default: ""
    },
    country: {
        type: String,
        default: ""
    },
    orders:
        [{ type: Schema.Types.ObjectId, ref: 'Order' }]


})
// Biên dịch Schema
module.exports = mongoose.model("Customer", customerSchema)