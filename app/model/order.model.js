// Import Thư viện Express
const dayjs = require("dayjs")
const mongoose = require("mongoose")
// Khai báo Schema
const Schema = mongoose.Schema
const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';



// Khởi tạo schema
const orderSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    orderDate: {
        type: Date,
        default: Date.now()
    },
    orderCode: {
        type:String
    },
    shippedDate: {
        type: Date
    },
    note: {
        type: String
    },
    orderDetails:
        [{ type: Schema.Types.ObjectId, ref: 'OrderDetail' }],
    cost: {
        type: Number,
        default: 0
    }


})
// Biên dịch Schema
module.exports = mongoose.model("Order", orderSchema)