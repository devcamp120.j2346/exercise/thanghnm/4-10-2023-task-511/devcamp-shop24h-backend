const mongoose =require("mongoose")

const userModel = new mongoose.Schema({
    username:{
        type:String,
        unique:true,
        required:true
    },
    password:{
        type:String,
        required:true

    },
    email:{
        type:String,
        required:true,
        unique:true,

    },
    phone:{
        type:String,
        unique:true,
        required:true
    },
    role:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:"Role"
    }]
})
module.exports = mongoose.model("User",userModel)