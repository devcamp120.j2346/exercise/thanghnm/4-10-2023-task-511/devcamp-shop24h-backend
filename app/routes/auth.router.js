const express = require("express")
const { checkDublicateUsername, verifyToken } = require("../middleware/auth.middleware")
const { signUp, logIn, refreshToken, getUserById, getAllUser } = require("../controllers/auth.controller")
const route = express.Router()

route.get("/users/:userId",getUserById)
route.get("/users",getAllUser)
route.post("/signup",checkDublicateUsername,signUp)
route.post("/login",logIn)
route.post("/refresh/token",verifyToken,refreshToken)   
module.exports = route
