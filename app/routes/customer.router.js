// Import Thư viện Express
const express = require("express");
// Import Controller
const { createCustomer, getAllCustomer, getCustomerById, updateCustomerById, deleteCustomerById } = require("../controllers/customer.controller");
// Khai báo router
const router = express.Router();

// Router tạo customer customer
router.post("/", createCustomer)
// Router lấy danh sách customer
router.get("/", getAllCustomer)
// Router lấy customer bằng Id
router.get("/:customerId/", getCustomerById)
// Router update customer bằng Id
router.put("/:customerId/", updateCustomerById)
// Router xóa customer bằng Id
router.delete("/:customerId/", deleteCustomerById)
// Export
module.exports = router
