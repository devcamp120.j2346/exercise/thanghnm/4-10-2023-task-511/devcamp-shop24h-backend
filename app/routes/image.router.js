var imgSchema = require('../model/image.model');
var path = require('path');
var fs = require('fs');
// Import Thư viện Express
const express = require("express");
// Import Controller
var multer = require('multer');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
        cb(null, "image" + '-' + Date.now())
    }
});
var upload = multer({ storage: storage });
// Khai báo router
const router = express.Router();
router.get('/', async (req, res) => {
   var result= await imgSchema.find()
        .then((data, err) => {
            if (err) {
                console.log(err);
            }
            res.render('imagepage', { items: data })
            // return  res.status(200).json({
            //     result
            // })
        })
});
router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        var obj = {
            name: req.body.name,
            desc: req.body.desc,
            img: {
                data: fs.readFileSync(path.join('C:/Users/ASUS/Desktop/J2346/4-10-2023 Project 24H/shop24h-BE/uploads/' + req.body.ImageUrl)),
                contentType: 'image/png'
            }
        }
       var result= await imgSchema.create(obj)
            return  res.status(201).json({
                    result:result._id
                })
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

});


// Export
module.exports = router
