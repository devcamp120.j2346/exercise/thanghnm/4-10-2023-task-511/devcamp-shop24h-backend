// Import Thư viện Express
const express = require("express");
const { createOrderOfCustomer, getAllOrder, getAllOrderOfCustomer, getOrderById, updateOrderById, deleteOrderById } = require("../controllers/order.controller");
// Import Controller
// Khai báo router
const router = express.Router();

// Router tạo order và add vào customer 
router.post("/:customerId", createOrderOfCustomer)
// Router lấy danh sách order
router.get("/", getAllOrder)
// Router lấy danh sách order của customer
router.get("/:customerId", getAllOrderOfCustomer)
// Router lấy order bằng Id
router.get("/all/:orderId", getOrderById)
// Router update order bằng Id
router.put("/:orderId", updateOrderById)
// // Router xóa order bằng Id
router.delete("/:orderId", deleteOrderById)
// Export
module.exports = router
