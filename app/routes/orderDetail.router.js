// Import Thư viện Express
const express = require("express");
const { createDetailOfOrder, getAllDetailOrder, getAllDetailsOfOrder, getOrderDetailById, updateDetailById, deleteDetailById } = require("../controllers/orderDetail.controller");
// Import Controller
// Khai báo router
const router = express.Router();

// Router tạo detail 
router.post("/:orderId/", createDetailOfOrder)
// Router lấy danh sách detail
router.get("/all/", getAllDetailOrder)
// Router lấy danh sách detail bằng Id
router.get("/:orderId/", getAllDetailsOfOrder)
// Router lấy detail bằng Id
router.get("/id/:detailId", getOrderDetailById)
// Router update detail bằng Id
router.put("/id/:detailId", updateDetailById)
// Router xóa detail bằng Id
router.delete("/id/:detailId", deleteDetailById)
// Export
module.exports = router
