// Import Thư viện Express
const express = require("express");
// Import Controller
const { createProduct, getAllProduct, getProductById, updateProductById, deleteProductById, } = require("../controllers/product.controller");
// Khai báo router
const router = express.Router();

// Router tạo product product
router.post("/", createProduct)
// // Router lấy danh sách product
router.get("/", getAllProduct)
// // Router lấy product bằng Id
router.get("/:productId/", getProductById)
// // Router update product bằng Id
router.put("/:productId/", updateProductById)
// // Router xóa product bằng Id
router.delete("/:productId/", deleteProductById)
// Export
module.exports = router
