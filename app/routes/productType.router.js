// Import Thư viện Express
const express = require("express");
// Import Controller
const { createProductType, getAllType, getTypeById, updateTypeById, deleteTypeById } = require("../controllers/productType.controller");
// Khai báo router
const router = express.Router();

// Router tạo product type
router.post("/",createProductType)
// Router lấy danh sách type
router.get("/",getAllType)
// Router lấy type bằng Id
router.get("/:typeId/",getTypeById)
// Router update type bằng Id
router.put("/:typeId/",updateTypeById)
// Router xóa type bằng Id
router.delete("/:typeId/",deleteTypeById)
//
module.exports=router
