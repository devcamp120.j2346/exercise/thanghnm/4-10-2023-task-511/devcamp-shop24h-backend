
module.exports = {
    port: 8000,
    db: {
      production: "mongodb://localhost:27017/CRUD_Shop24h",
      development: "mongodb://localhost:27017/CRUD_Shop24h",
      test: "mongodb://localhost:27017/CRUD_Shop24h",
    },
    dbParams: {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true
    }
};
