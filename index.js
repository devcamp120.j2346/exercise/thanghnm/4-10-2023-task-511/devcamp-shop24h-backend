// Import Thư viện Express
const express = require("express")
require('dotenv').config();
var bodyParser = require('body-parser');
// Import Thư viện Mongoose
const mongoose = require("mongoose")
// Khởi tạo app Express
const app = new express()
// Khởi tạo cổng
const port = process.env.PORT
const cors=require('cors')
// Cấu hình để sử dụng Json
app.use(express.json())
// Khai báo kết nối MongoDB
mongoose.connect(process.env.MONGO_URL)
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch(error => console.log(error))
//CORS
app.set("view engine", "ejs");
app.use(cors({ origin: true }))
//
// Import Router
const productTypeRouter = require("./app/routes/productType.router")
const productRouter = require("./app/routes/product.router")
const customerRouter = require("./app/routes/customer.router")
const orderRouter = require("./app/routes/order.router")
const orderDetailRouter = require("./app/routes/orderDetail.router")
const roleRouter = require("./app/routes/role.router")
const authRouter = require("./app/routes/auth.router")
const imageRouter = require("./app/routes/image.router")
// Sử dụng router
app.use(process.env.FIRST_PATH_API+"/types", productTypeRouter);
app.use(process.env.FIRST_PATH_API+"/products", productRouter);
app.use(process.env.FIRST_PATH_API+"/customers", customerRouter);
app.use(process.env.FIRST_PATH_API+"/orders", orderRouter);
app.use(process.env.FIRST_PATH_API+"/orderdetails", orderDetailRouter);
app.use(process.env.FIRST_PATH_API+"/roles", roleRouter);
app.use(process.env.FIRST_PATH_API+"/auth", authRouter);
app.use(process.env.FIRST_PATH_API+"/images", imageRouter);
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Start app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})
module.exports = app