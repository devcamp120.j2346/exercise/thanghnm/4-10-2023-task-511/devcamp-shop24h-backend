const request = require("supertest");
const customerModel = require("../app/model/customer.model");
const app = require("../index");
const mongoose = require('mongoose');
// const config = require('../config');
// const env = process.env.NODE_ENV || 'development';
// const typeId = ""
const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
// describe("MongoDB Connection", () => {
//     it("should be able to connect to MongoDB", (done) => {
//         const connectionState = mongoose.connection.readyState;
//         connectionState.should.eql(1); // 1 - đang kết nối
//         done();

//         // // 1 trong 2 cách
//         // chai.request("http://localhost:27017") // Thay đổi URL và cổng tương ứng
//         //     .get("/")
//         //     .end(function (err, res) {
//         //         res.status.should.eql(200);
//         //         done();
//         //     });
//     });

// });

describe("Test CRUD ProductType Restful API", () => {
    describe("/POST/ - Create", () => {
        after(async () => {
            const  condition = {}
            condition.email="email@gm.vn"
            await customerModel.findOneAndDelete(condition);
        });

        it("should create a new customer", async () => {
            const res = await chai
                .request(app)
                .post("/api/v1/customers/")
                .send({
                    _id: new mongoose.Types.ObjectId,
                    fullName:"fname",
                    phone:"0909090909",
                    email: "email@gm.vn",
                    address:"address",
                    city:"city",
                    country:"country",
                    orders:"65361ea6f148dd58940e737d"
                });

            res.should.have.status(201);
            // Kiểm tra dữ liệu được tạo
            res.body.should.be.a("object");
            res.body.should.have.property("result");
            res.body.result.should.have.property("fullName").eql("fname");
            res.body.result.should.have.property("email").eql("email@gm.vn");
        });
    });
})

// Test request Get /
describe("/GET/ - Get all", () => {
    it("should return all customer must be array", (done) => {
        chai.request(app)
            .get("/api/v1/customers/")
            .end((err, res) => {
                res.status.should.eql(200);
                res.body.result.should.be.a("array");
                done();
            });
    });
});
// Test request Get by Id
describe("/GET/customer/:id - Get one", () => {
    after(async () => {
      const  condition = {}
        condition.email="email@gm.vn"
        await customerModel.findOneAndDelete(condition);
    });

    it("should retrieve a productType by its ID", async () => {
        const customer = await customerModel.create({
            _id: new mongoose.Types.ObjectId,
            fullName:"fname",
            phone:"0909090901",
            email: "email@gm.vn",
            address:"address",
            city:"city",
            country:"country",
            orders:"65361ea6f148dd58940e737d"
        });

        const res = await chai
            .request(app)
            .get("/api/v1/customers/" + customer._id);

        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("result");
        res.body.result.should.have.property("fullName").eql("fname");
        res.body.result.should.have.property("email").eql("email@gm.vn");
        res.body.result.should.have
            .property("_id")
            .eql(customer._id.toString());
    });
});


// Test request update by Id
describe("PUT /customer/:id", () => {
    after(async () => {
        const  condition = {}
        condition.email="email@gm.vn"
        await customerModel.findOneAndDelete(condition);
    });

    it("should update a Customer by id", async () => {
        const updateData = await customerModel.create({
            _id: new mongoose.Types.ObjectId,
            fullName:"fname",
            phone:"0909090909",
            email: "email@gm.vn",
            address:"address",
            city:"city",
            country:"country",
            orders:"65361ea6f148dd58940e737d"
        });

        const res = await chai
            .request(app)
            .put("/api/v1/customers/" + updateData._id)
            .send({
                fullName:"fname updated",
                phone:"0909090909",
                email: "email@gm.vn",
                address:"address updated",
                city:"city updated",
                country:"country updated",
                orders:"65361ea6f148dd58940e737d"
            });
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("result");
        res.body.result.should.have.property("fullName")
    });
});
describe("/DELETE/customers/:id - Delete one", () => {
    it("should delete a produceType by id", async () => {
        let customer = await customerModel.create({
            _id: new mongoose.Types.ObjectId,
            fullName:"fname",
            phone:"0909090909",
            email: "email@gm.vn",
            address:"address",
            city:"city",
            country:"country",
            orders:"65361ea6f148dd58940e737d"
        });

        const res = await chai
            .request(app)
            .delete("/api/v1/customers/" + customer._id);

        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("message");

    });
});
