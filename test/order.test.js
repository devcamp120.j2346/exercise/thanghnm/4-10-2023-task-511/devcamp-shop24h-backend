const request = require("supertest");
const orderModel = require("../app/model/order.model");
const app = require("../index");
const mongoose = require('mongoose');
const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);

// describe("MongoDB Connection", () => {
//   it("should be able to connect to MongoDB", (done) => {
//       const connectionState = mongoose.connection.readyState;
//       connectionState.should.eql(1); // 1 - đang kết nối
//       done();

//       // // 1 trong 2 cách
//       // chai.request("http://localhost:27017") // Thay đổi URL và cổng tương ứng
//       //     .get("/")
//       //     .end(function (err, res) {
//       //         res.status.should.eql(200);
//       //         done();
//       //     });
//   });

// });

describe("Test CRUD order Restful API", () => {
    describe("/POST/order - Create", () => {
        after(async () => {
            // sắp xếp theo giá trị insert cuối cùng trước và xoá kết quả đầu tiên tìm thấy
            const condition = {}
            condition.note = "note"
            await orderModel.findOneAndDelete(condition);
        });

        it("should create a new product", async () => {
            const customerId = "651e7e350f668470ea4c335d"
            const res = await chai
                .request(app)
                .post("/api/v1/orders/" + customerId)
                .send({
                    _id: new mongoose.Types.ObjectId,
                    note: "note",
                    orderDetails: "65361ea6f148dd58940e737d",
                    cost: 1
                });

            res.should.have.status(201);
            // Kiểm tra dữ liệu được tạo
            res.body.should.be.a("object");
            res.body.should.have.property("result");
            res.body.result.should.have.property("note").eql("note");
            res.body.result.should.have.property("cost").eql(1);
        });
    });
})

// Test request Get /
describe("/GET/ - Get all", () => {
    it("should return all order must be array", (done) => {
        chai.request(app)
            .get("/api/v1/orders/")
            .end((err, res) => {
                res.status.should.eql(200);
                res.body.result.should.be.a("array");
                done();
            });
    });
});
// Test request Get by Id
describe("/GET/:id - Get one orders", () => {
    after(async () => {
        const condition = {}
        condition.note = "note"
        await orderModel.findOneAndDelete(condition);
    });

    it("should retrieve a product by its ID", async () => {
        const order = await orderModel.create({
            _id: new mongoose.Types.ObjectId,
            note: "note",
            orderDetails: "65361ea6f148dd58940e737d",
            cost: 1
        });

        const res = await chai
            .request(app)
            .get("/api/v1/orders/all/" + order._id);

        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("result");
        res.body.result.should.have.property("note").eql("note");
        res.body.result.should.have.property("cost").eql(1);
        res.body.result.should.have
            .property("_id")
            .eql(order._id.toString());
    });
});


// Test request update by Id
describe("PUT /product/:id", () => {
    after(async () => {
        const condition = {}
        condition.note = "note updated"
        await orderModel.findOneAndDelete(condition);
    });

    it("should update a productType by id", async () => {
        const updateData = await orderModel.create({
            _id: new mongoose.Types.ObjectId,
            note: "note",
            orderDetails: "65361ea6f148dd58940e737d",
            cost: 1
        });
        const res = await chai
            .request(app)
            .put("/api/v1/orders/" + updateData._id)
            .send({
                note: "note updated",
                orderDetails: "65361ea6f148dd58940e737d",
                cost: 1
            });
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("result");
        res.body.result.should.have.property("note");
        res.body.result.should.have.property("cost");
    });
});

describe("/DELETE/:id - Delete one", () => {
    it("should delete a product by id", async () => {
        let order = await orderModel.create({
            _id: new mongoose.Types.ObjectId,
            note: "note",
            orderDetails: "65361ea6f148dd58940e737d",
            cost: 1
        });

        const res = await chai
            .request(app)
            .delete("/api/v1/orders/" + order._id);

        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("message");

    });
});
