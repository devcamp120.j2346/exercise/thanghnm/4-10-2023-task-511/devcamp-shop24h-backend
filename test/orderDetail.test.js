const request = require("supertest");
const orderDetailModel = require("../app/model/orderDetail.model");
const app = require("../index");
const mongoose = require('mongoose');
const chai = require("chai");
const chaiHttp = require("chai-http");
const orderModel = require("../app/model/order.model");
const should = chai.should();
chai.use(chaiHttp);
// describe("MongoDB Connection", () => {
//     it("should be able to connect to MongoDB", (done) => {
//         const connectionState = mongoose.connection.readyState;
//         connectionState.should.eql(1); // 1 - đang kết nối
//         done();

//         // // 1 trong 2 cách
//         // chai.request("http://localhost:27017") // Thay đổi URL và cổng tương ứng
//         //     .get("/")
//         //     .end(function (err, res) {
//         //         res.status.should.eql(200);
//         //         done();
//         //     });
//     });

// });

describe("Test CRUD Order Detail Restful API", () => {
    describe("/POST/orderdetails - Create", () => {
        after(async () => {
            // sắp xếp theo giá trị insert cuối cùng trước và xoá kết quả đầu tiên tìm thấy
            const  condition = {}
            condition.product="655483d3b703a749418bc4f9"
            await orderDetailModel.findOneAndDelete(condition).sort({product:-1})
            // await orderModel.findOneAndUpdate("656c9a2d464d45e2dab24552",({"orderDetails":orderDetails.pop() }))
            ;
        });

        it("should create a new orderDetail", async () => {
            const res = await chai
                .request(app)
                .post("/api/v1/orderdetails/656c9a2d464d45e2dab24552")
                .send({
                    _id: new mongoose.Types.ObjectId,
                    product:"655483d3b703a749418bc4f9",
                    quantity:1
                });

            res.should.have.status(201);
            // Kiểm tra dữ liệu được tạo
            res.body.should.have.property("result");
            res.body.result.should.have.property("quantity").eql(1);
            res.body.result.should.have.property("product").eql("655483d3b703a749418bc4f9");
        });
    });
})

// Test request Get /
describe("/GET/orderDetail/ - Get all", () => {
    it("should return all Order detail must be array", (done) => {
        chai.request(app)
            .get("/api/v1/orderdetails/all")
            .end((err, res) => {
                res.status.should.eql(200);
                res.body.result.should.be.a("array");
                done();
            });
    });
});
// Test request Get by Id
describe("/GET/orderdetails/:id - Get one", () => {
    after(async () => {
        const condition = {}
        condition.product="65361ea6f148dd58940e737d"
        await orderDetailModel.findOneAndDelete(condition);
    });

    it("should retrieve a productType by its ID", async () => {
        const orderDetail = await orderDetailModel.create({
            _id: new mongoose.Types.ObjectId,
            quantity:1,
            product:"65361ea6f148dd58940e737d"
        });
        const res = await chai
            .request(app)
            .get("/api/v1/orderdetails/id/" + orderDetail._id);
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("result");
        res.body.result.should.have.property("product").eql("65361ea6f148dd58940e737d");
        res.body.result.should.have
            .property("_id")
            .eql(orderDetail._id.toString());
    });
});


// Test request update by Id
describe("PUT /orderdetails/:id", () => {
    after(async () => {
        const condition = {}
        condition.product="65361ea6f148dd58940e737d"
        await orderDetailModel.findOneAndDelete(condition);
    });

    it("should update a order detail by id", async () => {
        const updateData = await orderDetailModel.create({
            _id: new mongoose.Types.ObjectId,
            quantity:1,
            product:"65361ea6f148dd58940e737d"
        });
        const res = await chai
            .request(app)
            .put("/api/v1/orderdetails/id/" + updateData._id)
            .send({
                quantity:2,
            product:"65361ea6f148dd58940e737d"
            });
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("result");
        res.body.result.should.have.property("quantity")
    });
});
describe("/DELETE/orderdetails/:id - Delete one", () => {
    it("should delete a produceType by id", async () => {
        let orderDetail = await orderDetailModel.create({
            _id: new mongoose.Types.ObjectId,
            quantity:1,
            product:"65361ea6f148dd58940e737d"
        });

        const res = await chai
            .request(app)
            .delete("/api/v1/orderdetails/id/" + orderDetail._id);

        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("message");

    });
});
