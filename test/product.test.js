const request = require("supertest");
const productModel = require("../app/model/product.model");
const app = require("../index");
const mongoose = require('mongoose');
// const config = require('../config');
// const env = process.env.NODE_ENV || 'development';
// const typeId = ""
const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);

// describe("MongoDB Connection", () => {
//   it("should be able to connect to MongoDB", (done) => {
//       const connectionState = mongoose.connection.readyState;
//       connectionState.should.eql(1); // 1 - đang kết nối
//       done();

//       // // 1 trong 2 cách
//       // chai.request("http://localhost:27017") // Thay đổi URL và cổng tương ứng
//       //     .get("/")
//       //     .end(function (err, res) {
//       //         res.status.should.eql(200);
//       //         done();
//       //     });
//   });

// });

describe("Test CRUD product Restful API", () => {
    describe("/POST/ - Create", () => {
        after(async () => {
            // sắp xếp theo giá trị insert cuối cùng trước và xoá kết quả đầu tiên tìm thấy
            const  condition = {}
            condition.name="name"
            await productModel.findOneAndDelete(condition);
        });

        it("should create a new product", async () => {
            const res = await chai
                .request(app)
                .post("/api/v1/products/")
                .send({
                    _id: new mongoose.Types.ObjectId,
                    name: "name",
                    description: "desc",
                    type: "65361ea6f148dd58940e737d",
                    imageUrl: "test.vn",
                    buyPrice: 1,
                    promotionPrice: 1,
                    amount: 1
                });

            res.should.have.status(201);
            // Kiểm tra dữ liệu được tạo
            res.body.should.be.a("object");
            res.body.should.have.property("result");
            res.body.result.should.have.property("imageUrl").eql("test.vn");
            res.body.result.should.have.property("buyPrice").eql(1);
        });
    });
})

// Test request Get /
describe("/GET/ - Get all", () => {
    it("should return all product must be array", (done) => {
        chai.request(app)
            .get("/api/v1/products/")
            .end((err, res) => {
                res.status.should.eql(200);
                res.body.result.should.be.a("array");
                done();
            });
    });
});
// Test request Get by Id
describe("/GET/:id - Get one product", () => {
    after(async () => {
        const  condition = {}
        condition.name="name"
        await productModel.findOneAndDelete(condition);
    });

    it("should retrieve a product by its ID", async () => {
        const product = await productModel.create({
            _id: new mongoose.Types.ObjectId,
            name: "name",
            description: "desc",
            type: "65361ea6f148dd58940e737d",
            imageUrl: "test.vn",
            buyPrice: 1,
            promotionPrice: 1,
            amount: 1
        });

        const res = await chai
            .request(app)
            .get("/api/v1/products/" + product._id);

        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("result");
        res.body.result.should.have.property("type");
        res.body.result.should.have.property("imageUrl");
        res.body.result.should.have
            .property("_id")
            .eql(product._id.toString());
    });
});


// Test request update by Id
describe("PUT /product/:id", () => {
    after(async () => {
        const  condition = {}
        condition.name="name updated"
        await productModel.findOneAndDelete(condition);
    });

    it("should update a productType by id", async () => {
        const updateData = await productModel.create({
            _id: new mongoose.Types.ObjectId,
            name: "name",
            description: "desc",
            type: "65361ea6f148dd58940e737d",
            imageUrl: "test.vn",
            buyPrice: 1,
            promotionPrice: 1,
            amount: 1
        });
        const res = await chai
            .request(app)
            .put("/api/v1/products/" + updateData._id)
            .send({
                name: "name updated",
                description: "desc updated",
                type: "65361ea6f148dd58940e737d",
                imageUrl: "test.vn",
                buyPrice: 1,
                promotionPrice: 1,
                amount: 1
            });
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("result");
        res.body.result.should.have.property("name");
        res.body.result.should.have.property("type");
        res.body.result.should.have.property("imageUrl");
    });
});

describe("/DELETE/:id - Delete one", () => {
    it("should delete a product by id", async () => {
        let product = await productModel.create({
            _id: new mongoose.Types.ObjectId,
            name: "name",
            description: "desc",
            type: "65361ea6f148dd58940e737d",
            imageUrl: "test.vn",
            buyPrice: 1,
            promotionPrice: 1,
            amount: 1
        });

        const res = await chai
            .request(app)
            .delete("/api/v1/products/" + product._id);

        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("message");

    });
});
