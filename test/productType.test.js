const request = require("supertest");
const productType = require("../app/model/productType.model");
const app = require("../index");
const mongoose = require('mongoose');
// const config = require('../config');
// const env = process.env.NODE_ENV || 'development';
// const typeId = ""
const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();
chai.use(chaiHttp);
describe("MongoDB Connection", () => {
    it("should be able to connect to MongoDB", (done) => {
        const connectionState = mongoose.connection.readyState;
        connectionState.should.eql(1); // 1 - đang kết nối
        done();

        // // 1 trong 2 cách
        // chai.request("http://localhost:27017") // Thay đổi URL và cổng tương ứng
        //     .get("/")
        //     .end(function (err, res) {
        //         res.status.should.eql(200);
        //         done();
        //     });
    });

});

describe("Test CRUD ProductType Restful API", () => {
    describe("/POST/ - Create", () => {
        after(async () => {
            // sắp xếp theo giá trị insert cuối cùng trước và xoá kết quả đầu tiên tìm thấy
            await productType.findOneAndDelete({}, { sort: { _id: -1 } });
        });

        it("should create a new product type", async () => {
            const res = await chai
                .request(app)
                .post("/api/v1/types/")
                .send({
                    name: "name",
                    description: "desc",
                });

            res.should.have.status(201);
            // Kiểm tra dữ liệu được tạo
            res.body.should.be.a("object");
            res.body.should.have.property("result");
            res.body.result.should.have.property("description").eql("desc");
        });
    });
})

// Test request Get /
describe("/GET/ - Get all", () => {
    it("should return all productType must be array", (done) => {
        chai.request(app)
            .get("/api/v1/types/")
            .end((err, res) => {
                res.status.should.eql(200);
                res.body.result.should.be.a("array");
                done();
            });
    });
});
// Test request Get by Id
describe("/GET/:id - Get one", () => {
    after(async () => {
        await productType.findOneAndDelete({}, { sort: { _id: -1 } });
    });

    it("should retrieve a productType by its ID", async () => {
        const type = await productType.create({
            _id: new mongoose.Types.ObjectId,
            name: "name",
            description: "desc",
        });

        const res = await chai
            .request(app)
            .get("/api/v1/types/" + type._id);

        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("result");
        res.body.result.should.have
            .property("_id")
            .eql(type._id.toString());
    });
});


// Test request update by Id
describe("PUT /:typeId", () => {
    after(async () => {
        await productType.findOneAndDelete({}, { sort: { _id: -1 } });
    });

    it("should update a productType by id", async () => {
        const updateData = await productType.create({
            _id: new mongoose.Types.ObjectId,
            name: "name",
            description: "desc",
        });

        const res = await chai
            .request(app)
            .put("/api/v1/types/" + updateData._id)
            .send({
                name: "name updated",
                description: "desc updated",
            });
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("result");
        res.body.result.should.have.property("name")
    });
});
describe("/DELETE/:id - Delete one", () => {
    it("should delete a produceType by id", async () => {
        let type = await productType.create({
            _id: new mongoose.Types.ObjectId,
            name: "name",
            description: "desc",
        });

        const res = await chai
            .request(app)
            .delete("/api/v1/types/" + type._id);

        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.should.have.property("message");

    });
});
